# Obsidian
A [Foundry VTT](http://foundryvtt.com/) module that provides a dark theme character sheet for 5th edition D&D, incorporating concepts from [D&D Beyond](https://dndbeyond.com) and [Shaped](https://github.com/mlenser/roll20-character-sheets/tree/master/5eShaped).

![example screenshot](https://bitbucket.org/Fyorl/obsidian/raw/master/example.jpg)

## Bug Reports
If you discover an issue with the sheet, please report it in the [issue tracker](https://bitbucket.org/Fyorl/obsidian/issues) and ensure you include the following information:

1. The steps you took to encounter the issue.
2. What you expected to happen.
3. What actually happened.
4. Whether you encountered the issue in the foundry client or in a web browser, or both (and which browser and OS, if applicable).

In addition to the above information, it can be useful if you are able to do the following, too:

1. Press F12 to bring up the console and copy and paste any error messages you find there into the bug report.
2. Zip up your `actors.db` file and attach it to the report. It can be found in your foundry data directory under `Data/worlds/yourworld/data/actors.db`.

## License
All source code (`*.html`, `*.css`, `*.js`, `*.json`) is made available under the terms of the [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html). A copy of this license is available in the `LICENSE` file.

The fonts Roboto and Roboto Condensed found in the `fonts` directory are used and redistributed under the terms of the [Apache License v2](http://www.apache.org/licenses/LICENSE-2.0). A copy of this license is available in the `fonts/LICENSE-2.0.txt` file.

The font Noto Sans JP found in the `fonts` directory is used and redistributed under the terms of the [OFL 1.1](https://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web). A copy of this license is available in the `fonts/OFL.txt` file.

The following images found in the `img` directory are used and redistributed under the terms of [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/), with their attributions appearing here:

* [Delapouite](http://delapouite.com/): `versatile.svg`, `healing.svg`, `boots.svg`, `gauntlet.svg`, `bracers.svg`, `cloak.svg`, `belt.svg`, `ring.svg`, `gear.svg`, `ammo.svg`, `trinket.svg`, `provides-spells.svg`, `roll-modifier.svg`, `filter.svg`, `petrified.png`, `produce-resource.svg`
* [Lorc](http://lorcblog.blogspot.com/): `melee.svg`, `ranged.svg`, `bludgeoning.svg`, `piercing.svg`, `slashing.svg`, `psychic.svg`, `radiant.svg`, `thunder.svg`, `poison.svg`, `acid.svg`, `cold.svg`, `force.svg`, `lightning.svg`, `weapon.svg`, `tool.svg`, `armour.svg`, `helm.svg`, `consumable.svg`, `amulet.svg`, `effect.svg`, `demolish.svg`, `resource.svg`, `damage.svg`, `save.svg`, `scaling.svg`, `target.svg`, `consume-resource.svg`, `scroll.svg`, `bonus.svg`, `duration.svg`, `npc.svg`, `unconscious.png`, `burning.png`, `charmed.png`, `frightened.png`, `poisoned.png`, `restrained.png`, `stunned.png`, `exhaustion.png`, `grappled.png`, `incapacitated.png`, `paralysed.png`, `concentrating.png`, `surprised.png`, `loot.svg`, `expression.svg`
* [Skoll](https://game-icons.net/): `unarmed.svg`, `necrotic.svg`, `wand.svg`, `blinded.png`, `deafened.png`
* [sbed](https://opengameart.org/content/95-game-icons): `fire.svg`, `dead.png`, `marked.png`, `prone.png`
* [Willdabeast](http://wjbstories.blogspot.com/): `rod.svg`

The following images in the `img` directory (`copper.png`, `silver.png`, `electrum.png`, `gold.png`, `platinum.png`, `obsidian.png`) are © [mikiko](https://mikiko.art) and are used and redistributed under the terms of [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

The symbols for the schools of magic are © Wizards of the Coast and were assembled into the font file `fonts/SchoolsOfMagic.otf` by [Pjotr Frank](https://www.dmsguild.com/product/217275/Symbols-to-Represent-the-8-Schools-of-Magic).
